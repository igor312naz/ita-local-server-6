<?php

namespace App\Controller;

use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/sign-up", name="app-sign-up")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return Response
     */
    public function signUpAction(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $error = null;
        $user = new User();
        $form = $this->createForm("App\Form\RegisterType", $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($user->getPassport(), $user->getEmail())) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    $data = [
                        'email' => $user->getEmail(),
                        'passport' => $user->getPassport(),
                        'password' => $user->getPassword(),
                        'roles' => $user->getRoles(),
                    ];

                    $apiContext->createClient($data);

                    $user = $userHandler->createNewUser($data);
                    $manager->persist($user);
                    $manager->flush();

                    return $this->redirectToRoute("ping");
                }
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage() . '  |||  ' . var_export($e->getResponse(), 1);
            }
        }

        return $this->render('sign_up.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }


    /**
     * @Route("/", name="ping")
     */
    public function indexAction(ApiContext $apiContext)
    {
        try {
            return new Response(var_export($apiContext->makePing(), true));
        } catch (ApiException $e) {
            return new Response('Error: ' . $e->getMessage());
        }
    }

    /**
     * @Route("/auth", name="auth")
     * @param UserRepository $userRepository
     * @param Request $request
     * @param UserHandler $userHandler
     * @param ApiContext $apiContext
     * @param ObjectManager $manager
     * @return Response
     */
    public function authAction(
        UserRepository $userRepository,
        Request $request,
        UserHandler $userHandler,
        ApiContext $apiContext,
        ObjectManager $manager
    )
    {
        $error = null;

        $form = $this->createForm("App\Form\AuthType");

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $user = $userRepository->getByCredentials(
                $data['password'],
                $data['email']
            );

            if ($user) {
                $userHandler->makeUserSession($user);
                return $this->redirectToRoute('ping');
            }

            try {
                if ($apiContext->checkClientCredentials(
                    $data['password'],
                    $data['email']
                )) {
                    $centralData = $apiContext->getClientByEmail($data['email']);

                    $user = $userHandler->createNewUser(
                        $centralData,
                        false
                    );
                    $manager->persist($user);
                    $manager->flush();
                    $userHandler->makeUserSession($user);
                    return $this->redirectToRoute('ping');
                } else {
                    $error = 'Ты не тот, за кого себя выдаешь';
                }
            } catch (ApiException $e) {
                $error = 'Что-то где-то пошло нетак';
            }
        }

        return $this->render(
            '/sign_in.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/qwe", name="qwe")
     * @param ApiContext $apiContext
     * @return Response
     * @throws ApiException
     */
    public function qweAction(ApiContext $apiContext)
    {
        $result = $apiContext->clientExists('some & passport', '123@123.ru');
        return new Response(var_export($result, true));
    }

    /**
     * @Route("/login/{afterDenied}", name="login")
     * @param null/string $afterDenied
     * @return Response
     */
    public function loginAction($afterDenied = null)
    {

        if ($afterDenied) {
            return new Response('Ошибка - доступа нет');
        }

        return new Response('доступа есть');
    }

    /**
     * @Route("/error", name="app_error")
     */
    public function errorAction() {
        return $this->render("error.html.twig");
    }

    /**
     * @Route("/user/{id}/profile", name="app_profile")
     * @param User $user
     * @ParamConverter("user", class="App\Entity\User")
     * @return Response
     */
    public function profileAction(User $user = null)
    {
        if(!$user) {
            $this->addFlash(
                'error',
                'Пользователь не найден'
            );
            return $this->redirectToRoute('app_error');
        }

        return $this->render('user/profile.html.twig');
    }
}
